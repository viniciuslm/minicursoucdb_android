package br.ucdb.minicursoandroid.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.ucdb.minicursoandroid.model.User;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by viniciuslm on 06/11/17.
 */

public class ApiClient {

    //Endereço da API
    public static final String API_BASE_URL = "http://192.168.1.16:8081/";

    //Configuração padrão sempre que usar o retrofit
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY));

    private static Gson gsonBuilder = new GsonBuilder().create();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder));

    public static Routes getClient() {
        Retrofit retrofit = builder.client(httpClient.build()).build();
        return retrofit.create(Routes.class);
    }
    //Fim da config padrão

    //Métodos possíveis que a API fornece
    public interface Routes {

        @POST("/user/save")
        Call<ResponseBody> saveUser(@Body User user);

        @POST("/user/authenticate")
        Call<ResponseBody> authenticateUser(@Body User user);

    }
}
