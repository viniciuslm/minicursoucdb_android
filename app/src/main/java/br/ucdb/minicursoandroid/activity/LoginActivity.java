package br.ucdb.minicursoandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import br.ucdb.minicursoandroid.R;
import br.ucdb.minicursoandroid.api.ApiClient;
import br.ucdb.minicursoandroid.model.User;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by viniciuslm on 05/11/17.
 */

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener {

    //Anotações de validação utilizando lib Saripaar
    @NotEmpty
    @Email
    EditText edtEmail;
    @NotEmpty
    @Password
    EditText edtPassword;
    Button btnLogin;
    Button btnRegister;

    private SweetAlertDialog alertDialog;
    private Validator validator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Definindo o layout da activity
        setContentView(R.layout.activity_login);

        //Inicializando validador da lib Saripaar
        validator = new Validator(this);
        validator.setValidationListener(this);

        //Indicando a qual view do layout cada componente declarado corresponde
        edtEmail = findViewById(R.id.user_email);
        edtPassword = findViewById(R.id.user_password);
        btnLogin = findViewById(R.id.btn_login);
        btnRegister = findViewById(R.id.btn_register);

        //Chamando o método de validação dos campos quando o botão login for pressionado
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        //Mudando para tela de registro de usuário quando o botão de registro for pressionado
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
    }

    void verifyCredentials() {
        //Mostrando dialog enquanto se conecta ao servidor para autenticar
        if (alertDialog == null || !alertDialog.isShowing()) {
            alertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        } else {
            alertDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
        }
        alertDialog.setCancelable(false);
        alertDialog.showContentText(false);
        alertDialog.showCancelButton(false);
        alertDialog.setTitleText(getString(R.string.verifying));
        alertDialog.show();

        //Criando objeto usuario para autenticação com os dados dos campos
        User user = new User();
        user.setEmail(edtEmail.getText().toString());
        user.setPassword(edtPassword.getText().toString());

        //Fazendo chamada a API para validar os dados
        ApiClient.getClient().authenticateUser(user).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    //Usuário foi autenticado com sucesso. Fechando o dialog e mudando tela para MainActivity
                    alertDialog.dismiss();
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                } else {
                    //Usuário ou senha inválido
                    showErrorAlert(getString(R.string.login_error_dialog_content));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //Alguma falha ocorreu, como um erro de conexão
                showErrorAlert(getString(R.string.connection_error));
            }
        });
    }

    //Método para mostrar AlertDialog de erro
    private void showErrorAlert(String message) {
        alertDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        alertDialog.setTitleText(getString(R.string.error_dialog_title));
        alertDialog.setContentText(message);
        alertDialog.setConfirmText(getString(R.string.retry));
        alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                verifyCredentials();
            }
        });
        alertDialog.setCancelText(getString(R.string.cancel));
        alertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                alertDialog.dismissWithAnimation();
            }
        });
    }

    //Ao validar corretamente, chama o metodo de verificar credenciais
    @Override
    public void onValidationSucceeded() {
        verifyCredentials();
    }

    //Codigo padrao informado pela lib ao dar erro de validação
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
