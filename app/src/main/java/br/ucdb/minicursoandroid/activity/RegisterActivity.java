package br.ucdb.minicursoandroid.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import br.ucdb.minicursoandroid.R;
import br.ucdb.minicursoandroid.api.ApiClient;
import br.ucdb.minicursoandroid.model.User;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by viniciuslm on 06/11/17.
 */

public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener {

    Toolbar toolbar;
    //Anotações de validação utilizando lib Saripaar
    @NotEmpty
    @Email
    TextInputEditText edtEmail;
    @NotEmpty
    @Password
    TextInputEditText edtPassword;
    Button btnRegister;
    Button btnBack;

    private SweetAlertDialog alertDialog;
    private Validator validator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Definindo o layout da activity
        setContentView(R.layout.activity_register);

        //Inicializando validador da lib Saripaar
        validator = new Validator(this);
        validator.setValidationListener(this);

        //Indicando a qual view do layout cada componente declarado corresponde
        toolbar = findViewById(R.id.toolbar);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        btnRegister = findViewById(R.id.btn_finish_registration);
        btnBack = findViewById(R.id.btn_back);

        //Definindo a toolbar e exibindo botão de voltar na mesma
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Chamando o método saveUser quando o botão de finalizar registro for pressionado
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        //Finalizando a activity, retornando para a anterior, quando o botão voltar for pressionado
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //Método chamado quando o botão de voltar da toolbar é pressionado
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    //Método para salvar usuário na API
    private void saveUser() {
        //Criando objeto usuario para enviar para a API salvar com os dados dos campos
        User user = new User();
        user.setEmail(edtEmail.getText().toString());
        user.setPassword(edtPassword.getText().toString());

        //Mostrando dialog enquanto enviamos o objeto para a API
        if (alertDialog == null || !alertDialog.isShowing()) {
            alertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        } else {
            alertDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
        }
        alertDialog.setCancelable(false);
        alertDialog.showContentText(false);
        alertDialog.showCancelButton(false);
        alertDialog.setTitleText(getString(R.string.saving_user));
        alertDialog.show();

        //Enviando o objeto para ser salvo
        ApiClient.getClient().saveUser(user).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    //Usuário foi registrado corretamente, vamos mostrar uma mensagem para avisar
                    //e fechar a tela voltando a de login
                    alertDialog.setTitleText(getString(R.string.saved));
                    alertDialog.setContentText(getString(R.string.user_saved));
                    alertDialog.setConfirmText(getString(R.string.dialog_ok));
                    alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    });
                    alertDialog.showCancelButton(false);
                    alertDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                } else {
                    //Usuário não foi registrado pois o email já está em uso
                    showErrorAlert(getString(R.string.user_already_exists));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //Alguma falha ocorreu, como um erro de conexão
                showErrorAlert(getString(R.string.connection_error));
            }
        });
    }

    //Método para mostrar AlertDialog de erro
    private void showErrorAlert(String message) {
        alertDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        alertDialog.setTitleText(getString(R.string.error_dialog_title));
        alertDialog.setContentText(message);
        alertDialog.setConfirmText(getString(R.string.retry));
        alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                saveUser();
            }
        });
        alertDialog.setCancelText(getString(R.string.cancel));
        alertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                alertDialog.dismissWithAnimation();
            }
        });
    }


    //Ao validar corretamente, chama o metodo de verificar credenciais
    @Override
    public void onValidationSucceeded() {
        saveUser();
    }

    //Codigo padrao informado pela lib ao dar erro de validação
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

}
