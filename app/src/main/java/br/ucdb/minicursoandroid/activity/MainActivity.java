package br.ucdb.minicursoandroid.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import br.ucdb.minicursoandroid.R;

/**
 * Created by viniciuslm on 06/11/17.
 */

public class MainActivity extends AppCompatActivity {

    Button btnBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Definindo o layout da activity
        setContentView(R.layout.activity_main);

        //Indicando a qual view do layout cada componente declarado corresponde
        btnBack = findViewById(R.id.btn_back);

        //Finalizando a activity, retornando para a anterior, quando o botão voltar for pressionado
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
